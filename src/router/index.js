import Vue from 'vue'
import Router from 'vue-router'

const Home = () => import(/* webpackChunkName: "home" */ '@/pages/index')
const ContactDetail = () => import(/* webpackChunkName: "contact" */ '@/pages/contact/index')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/contact/:id',
      name: 'ContactDetail',
      component: ContactDetail,
      props: true,
    },
    { path: '*', redirect: '/' },
  ],
})
