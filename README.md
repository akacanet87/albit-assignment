# albit-assignment

> Albit Assignment

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

---

> ##Project Setting

``` bash
# vue cli 설치
$ npm install -g @vue/cli

# init 형식의 template 가져오기 위해 vue cli init 설치
$ npm install -g @vue/cli-init

# 프로젝트 생성
$ vue init webpack albit-assignment

```

> ## `/` : 최상위 폴더

1. `.eslintrc.js` 파일에 lint 설정 추가

> ## `/src` 하위 폴더

1. ###`/assets`
    - `/styles/common.scss` : 공용 css 적용을 위한 style 파일
    
2. ###`/axios`
    - `/index.js` : `axios` 를 이용하여 api 요청 시 기본 설정을 만들기 위한 파일

3. ###`/components`
    - `/Button.vue` : 공용 Button Component, loading 표시, `class` 별 색상 분리
    - `/PopupLayout.vue` : 공용 Popup Layout Component, 각 Popup을 감싸며 팝업 닫기 및 Background fade 를 주기 위한 Component.
    
4. ###`/pages`
    - `/index.vue` : 홈 화면, 검색 부분과 테이블 및 페이징을 보여주는 페이지
    - `/contact/index.vue` : 연락처 정보를 보여주기 위한 상세 페이지    
    
5. ###`/router`
    - `/index.js` : `vue-router` 를 이용하여 페이지 이동하도록 설정한 파일. [LazyLoading](https://router.vuejs.org/kr/guide/advanced/lazy-loading.html) 에서 참조하여 component 와 page 연결. 유효한 경로 이외의 url 로 접근 시 홈으로 redirect 되도록 설정.

6. ### `App.vue`
    - 최상위 컴포넌트
    
7. ### `main.js`
    - App 의 기본 설정이 있는 파일, Router, Axios 등을 바인딩

> ## `/static` 하위 폴더

1. ###`/images`
    - 이미지 파일 위치

---
